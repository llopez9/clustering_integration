import os
import json
import cv2
import numpy as np
from scipy.spatial.distance import cosine

# Define the paths to the dataset and the JSON file
common_path = "/home/VICOMTECH/llopez/WorkSpace/Databases/Market-1501"
images_path = "v15.09.15"
json_path = "output"
folder_name = "bounding_box_test"

json_path = os.path.join(common_path, json_path, folder_name)
img_path = os.path.join(common_path, images_path, folder_name)
cluster_path = os.path.join(common_path, "cluster")

feature_vectors = {}
for filename in os.listdir(json_path):
    # Check if the file is a JSON file
    if not filename.endswith('.json'):
        continue

    # Open the JSON file and load the feature vector
    json_file_path = os.path.join(json_path, filename)
    with open(json_file_path, 'r') as f:
        feature_vector = json.load(f)

    # Convert the feature vector to a numpy array
    feature_vector = np.array(feature_vector)[0]
    norm_feat = feature_vector / np.sqrt(np.sum(feature_vector**2))

    filename = filename.removesuffix('.json')

    # Load the image
    img_name = filename + ".jpg"
    img_path = os.path.join(common_path, images_path, folder_name, img_name)
    img = cv2.imread(img_path)
    feature_vectors[filename] = (norm_feat, img)

# Loop over all images in the dataset
for query_filename, (query_features, query_img) in feature_vectors.items():
    # Compute the similarity scores with all other images
    # using cosine similarity
    similarity_scores = []
    for filename, (features, img) in feature_vectors.items():
        if filename == query_filename:
            continue
        similarity_score = 1 - cosine(query_features, features)
        similarity_scores.append((filename, similarity_score))
    similarity_scores.sort(key=lambda x: x[1], reverse=True)

    # Select the top 5 images with the highest similarity scores
    top_5_images = similarity_scores[:5]

    # Create a folder to save the query image and the top 5 images
    save_folder_name = os.path.join(cluster_path, query_filename)
    os.makedirs(save_folder_name, exist_ok=True)

    # Save the query image
    query_img_path = os.path.join(save_folder_name, "_query_image.jpg")
    cv2.imwrite(query_img_path, query_img)

    # Save the top 5 images with their similarity scores as the filenames
    for i, (filename, similarity_score) in enumerate(top_5_images):
        _, img = feature_vectors[filename]

        img_path = os.path.join(
            save_folder_name, f"{similarity_score:.4f}.jpg")
        cv2.imwrite(img_path, img)
