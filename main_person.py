from fastapi import FastAPI
from pydantic import BaseModel
from kafka import KafkaConsumer, KafkaProducer
from json import loads, dumps
from os import getenv
from threading import Thread
from clustering.clustering import Clusterizer, Candidate
from clustering.adapter import get_image_from_candidates
# from clustering.adapter import saveClustersInImages
from clustering.adapter import saveClusterInImagesWithScore
# from clustering.adapter import saveClusterImagesCentroids
from OsNet_features_extractor.script_feature_extractor import osNetExtractor

import uvicorn
import os.path
import json
import numpy as np
import vcd.types as types
import vcd.core as core
import pickle
import threading
import base64

import logging
formatting = "%(asctime)s: %(funcName)s - %(message)s"
logging.basicConfig(format=formatting,
                    level=logging.INFO,
                    filename="app.log")

app = FastAPI()

# to decide whether two clusters should be fused together.
fusion_threshold = 0.87
cThrL = 0.83  # to connect a robust cluster with a non-robust one
cThrH = 0.88  # to connect two robust clusters
max_num_connections = 10  # max num of connections allowed for a robust cluster
min_num_candidates = 3  # minimum num of samples 2 classify a cluster as robust
maxNClusters = 1000000
bestscore = 0.0
bestindex = 0


class img_data(BaseModel):
    timestamp: str
    guid: str
    mimeType: str
    imgData: str


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def create_producer(bootstrap_server):
    logging.info(f'Creating Kafka producer: {bootstrap_server})...')
    producer = KafkaProducer(
        bootstrap_servers=[bootstrap_server],
        value_serializer=lambda x: dumps(x).encode('utf-8'),
        max_request_size=9048576000
    )
    logging.info('Kafka producer successfully created...')
    return producer


def create_consumer(bootstrap_server, topic_name):
    logging.info(f'Creating Kafka consumer: {bootstrap_server}')
    logging.info(f'with topic {topic_name}')
    logging.info(divider)
    consumer = KafkaConsumer(
        topic_name,
        # value_deserializer: método utilizado para deserializar los datos.
        # En este caso, transforma los datos recibidos en JSON.
        value_deserializer=lambda m: loads(m.decode('utf-8')),
        # bootstrap_servers: listado de brokers de Kafka
        # Ex:'iabd-virtualbox:9092'
        bootstrap_servers=[bootstrap_server]
    )
    return consumer


def save_pickle(path, object):
    with open(path, 'wb') as f:
        pickle.dump(object, f, pickle.HIGHEST_PROTOCOL)
    logging.info("saved in: {}".format(path))


def load_pickle(path):
    with open(path, 'rb') as f:
        object = pickle.load(f)
    logging.info("lodding from: {}".format(path))
    return object


def start_clusterizer():
    if (os.path.isfile('mounted_dir/clusterizer.pickle')):
        clusterizer = load_pickle('mounted_dir/clusterizer.pickle')
    else:
        clusterizer = Clusterizer(
            maxNClusters,
            fusion_threshold,
            cThrL,
            cThrH,
            max_num_connections,
            min_num_candidates
        )
    return clusterizer


clusterizer = start_clusterizer()
lock = threading.Lock()
divider = '=' * 40

osNetimages = osNetExtractor()


formatting = "%(asctime)s: %(funcName)s - %(message)s"
logging.basicConfig(
    level=logging.INFO,
    filename="mounted_dir/app.log")
logging.info('Program started')
logging.info(divider)


def create_clusterizer_from_json_files(common_path, folder_name):
    global clusterizer
    global lock
    json_path = os.path.join(common_path, "output", folder_name)
    img_path = os.path.join(common_path, "v15.09.15", folder_name)
    image_count = 0
    for filename in os.listdir(json_path):
        # Check if the file is a JSON file
        if not filename.endswith('.json'):
            continue

        # Open the JSON file and load the feature vector
        json_file_path = os.path.join(json_path, filename)
        with open(json_file_path, 'r') as f:
            feature_vector = json.load(f)

        # Convert the feature vector to a numpy array
        feature_vector = np.array(feature_vector)[0]
        norm_feat = feature_vector / np.sqrt(np.sum(feature_vector**2))

        # Read in the image file and convert to base64
        img_filename = filename.replace('.json', '.jpg')
        img_file_path = os.path.join(img_path, img_filename)
        with open(img_file_path, 'rb') as f:
            image_data = f.read()
        image_base64 = base64.b64encode(image_data).decode('utf-8')

        cand = Candidate(
            uid=-1,
            rid=-1,
            tmpid=None,
            features=norm_feat,
            image=image_base64
        )
        image_count += 1
        # if (image_count > 5500):  # or image_count % 1000 == 0
        print(image_count)
        # Add the candidate to the clusterizer
        with lock:
            clusterizer.clusterize_new_candidate(cand)
    clusterizer.saveClusterImagesCentroids(5, "clusterizer_in_images/")


def subscribe_to_kafka_and_clusterize_all_images():
    KAFKA_SERVER = getenv('KAFKA_SERVER')
    topic_name = "face_gallery"
    producer = create_producer(bootstrap_server=KAFKA_SERVER)
    consumer = create_consumer(
        bootstrap_server=KAFKA_SERVER,
        topic_name=topic_name)
    global clusterizer
    global lock

    for message in consumer:
        temp_file_VCD_path = './temp_json_data.json'
        json_data = json.loads(message.value)
        logging.info(f"Received json : {json_data}")
        if 'output_data' in json_data:
            with open(temp_file_VCD_path, 'w') as f:
                # json para guardado temporal para crear el VCD
                json.dump(json_data['output_data']['data'], f)

            try:
                resourceID = json_data['resourceId']
                logging.info(f"Resource ID : {resourceID}")
                vcd_from_json = core.VCD(file_name=temp_file_VCD_path)
                frame_intervals = vcd_from_json.get_frame_intervals()
                frame_interval = [[ft['frame_start'], ft['frame_end']]
                                  for ft in frame_intervals]
                frame_interval = [
                    item for sublist in frame_interval for item in sublist]
                frame_interval.sort()
                frame_range = range(frame_interval[0], frame_interval[-1] + 1)
                if not frame_range:
                    frame_range = list(set(frame_interval))

                for current_frame in frame_range:
                    frame_object = vcd_from_json.get_frame(current_frame)
                    if not frame_object:
                        logging.info(
                            f'No Object is detected at frame {current_frame}')
                        continue
                    for uid in frame_object['objects'].keys():
                        face_uuid = vcd_from_json.get_object_data(
                            uid,
                            "face_uuid",
                            current_frame
                        )['val']
                        feature = np.array(vcd_from_json.get_object_data(
                            uid,
                            "feat_vector",
                            current_frame
                        )['val'])
                        norm_feat = feature / np.sqrt(np.sum(feature**2))
                        img64 = vcd_from_json.get_object_data(
                            uid,
                            "crop",
                            current_frame
                        )['val']
                        cand = Candidate(
                            uid=-1,
                            rid=face_uuid,
                            tmpid=None,
                            features=norm_feat,
                            image=img64,
                            resourceID=resourceID
                        )
                        logging.info("clustering uuid: " + face_uuid)

                        with lock:
                            clusterizer.clusterize_new_candidate(cand)
                            save_pickle(
                                'mounted_dir/clusterizer.pickle',
                                clusterizer)
                            clusters = clusterizer._query(cand, 5)
                            logging.info('clusters are retrieved ...')

                        reid_rank_top_5 = []
                        reid_rank_top_5_similarity = []
                        reid_rank_top_5_resource_id = []

                        for c in clusters:
                            closest_candidate_from_centroid = \
                                clusterizer.\
                                get_closest_candidate_from_centroid(
                                    c.cluster_centroid, c.cands)
                            reid_rank_top_5.append(
                                closest_candidate_from_centroid.realID)
                            reid_rank_top_5_similarity.append(c.sim_score)
                            reid_rank_top_5_resource_id.append(
                                closest_candidate_from_centroid.resourceID)
                            logging.info(
                                f"similarity to cluster {c.pid} \
                                of person {c.pid}:{c.sim_score}")

                        # adding the reid result into the vcd
                        vcd_from_json.add_object_data(uid, types.vec(
                            "reid_rank_top_5",
                            reid_rank_top_5),
                            current_frame)
                        vcd_from_json.add_object_data(uid, types.vec(
                            "reid_rank_top_5_similarity",
                            reid_rank_top_5_similarity),
                            current_frame)
                        vcd_from_json.add_object_data(uid, types.vec(
                            "reid_rank_top_5_resourceID",
                            reid_rank_top_5_resource_id),
                            current_frame)

                json_data['output_data']['data'] = vcd_from_json.data
                json_data['processing_type'] = "FACE_RECOG"
                output_json = dumps(json_data)
                producer.send('results', value=output_json)
                producer.flush()

            except Exception as ex:
                logging.exception(
                    f'Exception occurred when saving the image: {str(ex)}')
        else:
            logging.info('There is no vcd in the json file')


@app.post("/API/clean_clusterizer",
          summary='Delete all information inside the cluster')
async def clean_clusterizer():
    """
    Name: clean_clusterizer

    Sumary: Delete all information inside the cluster

    Parameters: None

    Response: None
    """
    logging.info(divider)
    logging.info("restarting the clusterizer")
    global clusterizer
    global lock
    with lock:
        clusterizer = Clusterizer(
            maxNClusters,
            fusion_threshold,
            cThrL,
            cThrH,
            max_num_connections,
            min_num_candidates)
        save_pickle('mounted_dir/clusterizer.pickle', clusterizer)

    logging.info("new clusterizer saved")
    logging.info(divider)
    return


@app.get("/API/save_clusterizer_in_images",
         summary='Distribution of folders with images separated by clusters')
async def save_clusterizer_in_images():
    """
    Name: save_clusterizer_in_images

    Summary: Distribution of folders with images separated by clusters

    Description: Internal endpoint to visualize the images by clusters

    Parameters: None

    Response: None / save all images in a internal forlder

        mount_dir
        |__cluster1
        |    |__image_1
        |    |__image_2
        |    |__...
        |    |__image_n
        |__cluster2
        |    |__image_1
        |...
    """
    logging.info("start")
    logging.info(divider)
    logging.info("Saving cluster in image with scores")
    logging.info("loading cluster")
    global clusterizer
    global lock
    with lock:
        saveClusterInImagesWithScore(
            clusterizer,
            "mounted_dir/clusterizer_in_images"
        )
    logging.info(divider)
    logging.info("end")
    return


@app.post("/API/SEARCH/closest_clusters",
          summary='Clusterize a new value and returns the nearest K clusters')
async def find_K_closest_clusters_from_json(json_data, num_of_neighbours=5):
    """
    Name: find_K_closest_clusters

    Summary: Based on the characteristics of a candidate provided,
    the search will be executed

    Description: From the information in the VCD_json, then the k clusters
                 closest to its characteristics are searched for and returned,
                 Note: the candidate is not saved within the cluster,
                 it is for inquiries only.

    Parameters:

    json_data -> a json VCD with the information,
                 like the ones in the kafka topics

    k -> the number of closest clusters desired for the search, by defoult is 5

    Response: A json file with the rank_k of the clusters

        like this:
        {
            "context_data":{
                "executionId":"3d22d57b-786c-40f1-b4a6-d5dcd90a12ef",

                "resourceId":"5c07e771-eb39-4dcd-9e7e-3b80ea517099",

                "startDate":"2022-10-05T14:20:53.745852Z",
            }
            "output_data":{

                "data":{

                    "message_type":"DATA",

                        "service_id":"1234",

                        "service_type":"FACE_GALLERY",

                        "datetime":"2023-06-12T08:04:23z",

                        "vcd":{

                            "text":[

                                {
                                    "name":"face_uuid",
                                    "val":"97f12e39-2603-398a-aad4-315ba0060155"
                                },

                                {
                                    "name":"class",
                                    "val":"face"
                                }
                            ],

                            "bbox":[
                                {
                                    "name":"bbox",
                                    "val":[1068, 393, 1138, 479]
                                }
                            ],

                            "num":[
                                {
                                    "name":"score",
                                    "val":0.8226723670959473
                                }
                            ],

                            "vec":[
                                {
                                    "name":"feat_vector",
                                    "val":[ values ]
                                },

                                {
                                    "name":"reid_rank_top_5",
                                    "val":[
                                        "97f12e39-2603-398a-aad4-315ba0060155",
                                        "0863d46f-25c3-3cd4-bdb8-2840d7c22153",
                                        "3d273a27-c672-3242-8e03-be7dfedb1272",
                                        "21ca0ba9-4369-3299-ad2f-8036a27b8518",
                                        "4523d361-4d49-3fdb-87a0-66d3f613bf9e"
                                    ]
                                },

                                {
                                    "name":"reid_rank_top_5_similarity",
                                    "val":[
                                        0.999988764443502,
                                        0.18887574393083154,
                                        0.15909059736188633,
                                        0.1535370461533556,
                                        0.1421205891187343
                                    ]
                                },

                                {
                                    "name":"reid_rank_top_5_resourceID",
                                    "val":[
                                        "5c07e771-eb39-4dcd-9e7e-3b80ea517099",
                                        "5c07e771-eb39-4dcd-9e7e-3b80ea517099",
                                        "5c07e771-eb39-4dcd-9e7e-3b80ea517099",
                                        "5c07e771-eb39-4dcd-9e7e-3b80ea517099",
                                        "5c07e771-eb39-4dcd-9e7e-3b80ea517099"
                                    ]
                                }
                            ],

                            "boolean":[
                                {
                                "name":"occlusion",
                                "val":False
                                }
                            ],

                            "image":[
                                {
                                    "name":"crop",
                                    "val":"image_base64_data",
                                    "mime_type":"image/png",
                                    "encoding":"base64"
                                }
                            ]
                        }
                }
            }
        }
    """
    temp_file_VCD_path = './temp_json_data.json'
    json_data = json.loads(json_data)
    if 'output_data' in json_data:
        with open(temp_file_VCD_path, 'w') as f:
            # json para guardado temporal para crear el VCD
            json.dump(json_data['output_data']['data'], f)

        try:
            resourceID = json_data['resourceId']
            logging.info(f"Resource ID : {resourceID}")
            vcd_from_json = core.VCD(file_name=temp_file_VCD_path)
            frame_intervals = vcd_from_json.get_frame_intervals()
            frame_interval = [[ft['frame_start'], ft['frame_end']]
                              for ft in frame_intervals]
            frame_interval = [
                item for sublist in frame_interval for item in sublist]
            frame_interval.sort()
            frame_range = range(frame_interval[0], frame_interval[-1] + 1)
            if not frame_range:
                frame_range = list(set(frame_interval))

            for current_frame in frame_range:
                frame_object = vcd_from_json.get_frame(current_frame)
                if not frame_object:
                    logging.info(
                        f'No Object is detected at frame {current_frame}')
                    continue
                for uid in frame_object['objects'].keys():
                    face_uuid = vcd_from_json.get_object_data(
                        uid,
                        "face_uuid",
                        current_frame
                    )['val']
                    feature = np.array(vcd_from_json.get_object_data(
                        uid,
                        "feat_vector",
                        current_frame
                    )['val'])
                    norm_feat = feature / np.sqrt(np.sum(feature**2))
                    img64 = vcd_from_json.get_object_data(
                        uid,
                        "crop",
                        current_frame
                    )['val']
                    cand = Candidate(
                        uid=-1,
                        rid=face_uuid,
                        tmpid=None,
                        features=norm_feat,
                        image=img64,
                        resourceID=resourceID)
                    logging.info("clustering uuid: " + face_uuid)

                    with lock:
                        clusterizer.clusterize_new_candidate(cand)
                        save_pickle(
                            'mounted_dir/clusterizer.pickle',
                            clusterizer)
                        clusters = clusterizer._query(cand, 5)
                        logging.info('clusters are retrieved ...')

                    reid_rank_top_5 = []
                    reid_rank_top_5_similarity = []
                    reid_rank_top_5_resource_id = []

                    for c in clusters:
                        closest_candidate_from_centroid = \
                            clusterizer.get_closest_candidate_from_centroid(
                                c.cluster_centroid, c.cands)
                        reid_rank_top_5.append(
                            closest_candidate_from_centroid.realID)
                        reid_rank_top_5_similarity.append(c.sim_score)
                        reid_rank_top_5_resource_id.append(
                            closest_candidate_from_centroid.resourceID)
                        logging.info(
                            f"similarity to cluster {c.pid} of person {c.pid}:\
                                {c.sim_score}")

                    # adding the reid result into the vcd
                    vcd_from_json.add_object_data(uid, types.vec(
                        "reid_rank_top_5",
                        reid_rank_top_5),
                        current_frame)
                    vcd_from_json.add_object_data(uid, types.vec(
                        "reid_rank_top_5_similarity",
                        reid_rank_top_5_similarity),
                        current_frame)
                    vcd_from_json.add_object_data(uid, types.vec(
                        "reid_rank_top_5_resourceID",
                        reid_rank_top_5_resource_id),
                        current_frame)

            json_data['output_data']['data'] = vcd_from_json.data
            json_data['processing_type'] = "FACE_RECOG"
            # output_json = dumps(json_data)
            # producer.send('results', value=output_json)
            # producer.flush()

        except Exception as ex:
            logging.exception(
                f'Exception occurred when saving the image: {str(ex)}')
    else:
        logging.info('There is no vcd in the json file')


@app.get("/API/SEARCH/{uuid}",
         summary="Find nearest clusters to given subject")
async def find_K_closest_clusters_from_uuid(uuid: str, num_of_neighbours=5):
    """
    Name: Find nearest clusters to given subject

    Summary: based on the characteristics of a candidate provided,
    the search will be executed

    Description: It searches through all the images in the clusterizer
                 at that moment, and if this uuid exists,
                 returns the image.

    Parameters: uuid:sting

    Response: image_in_base64: string
    """
    pass


@app.get("/API/CROP/{uuid}",
         summary='Get a crop_image from the cluster')
async def get_image_with_uuid(uuid: str):
    """
    Name: get_image_with_uuid

    Summary: Get a crop_image from the cluster

    Description: It searches through all the images in the clusterizer
                 at that moment, and if this uuid exists,
                 returns the image.

    Parameters: uuid:sting

    Response: image_in_base64: string
    """
    logging.info(divider)
    logging.info("searsh crop for uuid:" + uuid)
    logging.info("loading cluster")
    global clusterizer
    global lock
    with lock:
        img64 = get_image_from_candidates(clusterizer, uuid)
    logging.info(divider)
    return (img64)


@app.get("/API/SEARCH/random",
         summary='clusterize a random image from Market1501_database')
async def clusterize_random_image():
    feature, id = osNetimages.get_random_FV()
    cand = Candidate(
        uid=-1,
        rid=id,
        features=feature
    )
    clusterizer.clusterize_new_candidate(cand)


@app.on_event("startup")
async def startup_event():
    global clusterizer
    global lock
    logging.info(divider)
    logging.info("main function start / subscribed to kafka")
    thread = Thread(target=subscribe_to_kafka_and_clusterize_all_images)
    thread.start()


if __name__ == '__main__':
    logging.info("FastApi is starting")
    uvicorn.run(app, host="0.0.0.0", port=9095)
    # common_path = "/home/VICOMTECH/llopez/WorkSpace/Databases/Market-1501"
    # folder_name = "bounding_box_train"
    # create_clusterizer_from_json_files(common_path, folder_name)
