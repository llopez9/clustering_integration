from fastapi import FastAPI
from pydantic import BaseModel
from kafka import KafkaConsumer, KafkaProducer
from json import loads, dumps
from os import getenv
from threading import Thread
from clustering.clustering import Clusterizer, Candidate

import uvicorn
import os.path
import json
import numpy as np
import vcd.types as types
import vcd.core as core
import pickle
import threading

# from PIL import Image
# import concurrent.futures
# import time
# import base64
# import cv2
# from multiprocessing.dummy import Array
# from unittest import result

import logging
formatting = "%(asctime)s: %(funcName)s - %(message)s"
logging.basicConfig(format=formatting,
                    level=logging.INFO,
                    filename="app.log")

app = FastAPI()

# to decide whether two clusters should be fused together.
fusion_threshold = 0.68
cThrL = 0.52  # to connect a robust cluster with a non-robust one
connectThrH = 0.74  # to connect two robust clusters
maxNConnections = 25  # max number of connections allowed for a robust cluster
min_num_candidates = 4  # mini number of samples to classify a c as robust
maxNClusters = 500
bestscore = 0.0
bestindex = 0


def save_pickle(path, object):
    with open(path, 'wb') as f:
        pickle.dump(object, f, pickle.HIGHEST_PROTOCOL)
    logging.info("saved in: {}".format(path))


def load_pickle(path):
    with open(path, 'rb') as f:
        object = pickle.load(f)
    logging.info("lodding from: {}".format(path))
    return object


if (os.path.isfile('mounted_dir/clusterizer.pickle')):
    clusterizer = load_pickle('mounted_dir/clusterizer.pickle')
else:
    clusterizer = Clusterizer(
        maxNClusters,
        fusion_threshold,
        cThrL,
        connectThrH,
        maxNConnections,
        min_num_candidates
    )

lock = threading.Lock()
divider = '=' * 40

formatting = "%(asctime)s: %(funcName)s - %(message)s"
logging.basicConfig(level=logging.INFO,
                    filename="mounted_dir/app.log")
logging.info('Program started .....')
logging.info(divider)


class img_data(BaseModel):
    timestamp: str
    guid: str
    mimeType: str
    imgData: str


def create_producer(bootstrap_server):
    logging.info(f'Creating Kafka producer: {bootstrap_server})...')
    producer = KafkaProducer(
        bootstrap_servers=[bootstrap_server],
        value_serializer=lambda x: dumps(x).encode('utf-8'),
        max_request_size=9048576000
    )
    logging.info('Kafka producer successfully created...')
    return producer


def create_consumer(bootstrap_server, topic_name):
    logging.info(f'Creating Kafka consumer: {bootstrap_server}')
    logging.info(f'with topic {topic_name}')
    logging.info(divider)
    consumer = KafkaConsumer(
        topic_name,
        # value_deserializer: método utilizado para deserializar los datos.
        # En este caso, transforma los datos recibidos en JSON.
        value_deserializer=lambda m: loads(m.decode('utf-8')),
        # bootstrap_servers: listado de brokers de Kafka
        # Ex:'iabd-virtualbox:9092'
        bootstrap_servers=[bootstrap_server]
    )
    return consumer


class NpEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, np.integer):
            return int(obj)
        elif isinstance(obj, np.floating):
            return float(obj)
        elif isinstance(obj, np.ndarray):
            return obj.tolist()
        else:
            return super(NpEncoder, self).default(obj)


def subscribe_to_kafka_and_clusterize_all_images():
    KAFKA_SERVER = getenv('KAFKA_SERVER')
    topic_name = "face_gallery"
    producer = create_producer(bootstrap_server=KAFKA_SERVER)
    consumer = create_consumer(
        bootstrap_server=KAFKA_SERVER,
        topic_name=topic_name)
    global clusterizer
    global lock

    for message in consumer:
        temp_file_VCD_path = './temp_json_data.json'
        json_data = json.loads(message.value)
        logging.info(f"Received json : {json_data}")
        if 'output_data' in json_data:
            with open(temp_file_VCD_path, 'w') as f:
                # json para guardado temporal para crear el VCD
                json.dump(json_data['output_data']['data'], f)

            try:
                resourceID = json_data['resourceId']
                logging.info(f"Resource ID : {resourceID}")
                vcd_from_json = core.VCD(file_name=temp_file_VCD_path)
                frame_intervals = vcd_from_json.get_frame_intervals()
                frame_interval = [[ft['frame_start'], ft['frame_end']]
                                  for ft in frame_intervals]
                frame_interval = [
                    item for sublist in frame_interval for item in sublist]
                frame_interval.sort()
                frame_range = range(frame_interval[0], frame_interval[-1] + 1)
                if not frame_range:
                    frame_range = list(set(frame_interval))

                for current_frame in frame_range:
                    frame_object = vcd_from_json.get_frame(current_frame)
                    if not frame_object:
                        logging.info(
                            f'No Object is detected at frame {current_frame}')
                        continue
                    for uid in frame_object['objects'].keys():
                        face_uuid = vcd_from_json.get_object_data(
                            uid,
                            "face_uuid",
                            current_frame
                        )['val']
                        feature = np.array(vcd_from_json.get_object_data(
                            uid,
                            "feat_vector",
                            current_frame
                        )['val'])
                        norm_feat = feature / np.sqrt(np.sum(feature**2))
                        img64 = vcd_from_json.get_object_data(
                            uid,
                            "crop",
                            current_frame
                        )['val']
                        cand = Candidate(
                            uid=-1,
                            rid=face_uuid,
                            tmpid=None,
                            features=norm_feat,
                            image=img64,
                            resourceID=resourceID)
                        logging.info("clustering uuid: " + face_uuid)

                        with lock:
                            clusterizer.clusterize_new_candidate(cand)
                            save_pickle(
                                'mounted_dir/clusterizer.pickle',
                                clusterizer)
                            clusters = clusterizer._query(cand, 5)
                            logging.info('clusters are retrieved ...')

                        reid_rank_top_5 = []
                        reid_rank_top_5_similarity = []
                        reid_rank_top_5_resource_id = []

                        for c in clusters:
                            closest_candidate_from_centroid = \
                                clusterizer.\
                                get_closest_candidate_from_centroid(
                                    c.cluster_centroid, c.cands)
                            reid_rank_top_5.append(
                                closest_candidate_from_centroid.realID)
                            reid_rank_top_5_similarity.append(c.sim_score)
                            reid_rank_top_5_resource_id.append(
                                closest_candidate_from_centroid.resourceID)
                            logging.info(
                                f"similarity to cluster {c.pid} \
                                of person {c.pid}:{c.sim_score}")

                        # adding the reid result into the vcd
                        vcd_from_json.add_object_data(uid, types.vec(
                            "reid_rank_top_5",
                            reid_rank_top_5),
                            current_frame)
                        vcd_from_json.add_object_data(uid, types.vec(
                            "reid_rank_top_5_similarity",
                            reid_rank_top_5_similarity),
                            current_frame)
                        vcd_from_json.add_object_data(uid, types.vec(
                            "reid_rank_top_5_resourceID",
                            reid_rank_top_5_resource_id),
                            current_frame)

                json_data['output_data']['data'] = vcd_from_json.data
                json_data['processing_type'] = "FACE_RECOG"
                output_json = dumps(json_data)
                producer.send('results', value=output_json)
                producer.flush()

            except Exception as ex:
                logging.exception(
                    f'Exception occurred when saving the image: {str(ex)}')
        else:
            logging.info('There is no vcd in the json file')


@app.post("/API/clean_clusterizer")
async def clean_clusterizer():
    logging.info(divider)
    logging.info("restarting the clusterizer")
    global clusterizer
    global lock
    with lock:
        clusterizer = Clusterizer(
            maxNClusters,
            fusion_threshold,
            cThrL,
            connectThrH,
            maxNConnections,
            min_num_candidates)
        save_pickle('mounted_dir/clusterizer.pickle', clusterizer)

    logging.info("new clusterizer saved")
    logging.info(divider)
    return


@app.post("/API/save_clusterizer_in_images")
async def save_clusterizer_in_images():
    logging.info("start")
    logging.info(divider)
    logging.info("Saving cluster in image with scores")
    logging.info("loading cluster")
    global clusterizer
    global lock
    with lock:
        clusterizer.saveClusterInImagesWithScore(
            "mounted_dir/clusterizer_in_images"
        )
    logging.info(divider)
    logging.info("end")
    return


@app.post("/API/find_K_closest_clusters")
async def find_K_closest_clusters(json_data, num_of_neighbours: int):
    temp_file_VCD_path = './temp_json_data.json'
    json_data = json.loads(json_data)
    if 'output_data' in json_data:
        with open(temp_file_VCD_path, 'w') as f:
            # json para guardado temporal para crear el VCD
            json.dump(json_data['output_data']['data'], f)

        try:
            resourceID = json_data['resourceId']
            logging.info(f"Resource ID : {resourceID}")
            vcd_from_json = core.VCD(file_name=temp_file_VCD_path)
            frame_intervals = vcd_from_json.get_frame_intervals()
            frame_interval = [[ft['frame_start'], ft['frame_end']]
                              for ft in frame_intervals]
            frame_interval = [
                item for sublist in frame_interval for item in sublist]
            frame_interval.sort()
            frame_range = range(frame_interval[0], frame_interval[-1] + 1)
            if not frame_range:
                frame_range = list(set(frame_interval))

            for current_frame in frame_range:
                frame_object = vcd_from_json.get_frame(current_frame)
                if not frame_object:
                    logging.info(
                        f'No Object is detected at frame {current_frame}')
                    continue
                for uid in frame_object['objects'].keys():
                    face_uuid = vcd_from_json.get_object_data(
                        uid,
                        "face_uuid",
                        current_frame
                    )['val']
                    feature = np.array(vcd_from_json.get_object_data(
                        uid,
                        "feat_vector",
                        current_frame
                    )['val'])
                    norm_feat = feature / np.sqrt(np.sum(feature**2))
                    img64 = vcd_from_json.get_object_data(
                        uid,
                        "crop",
                        current_frame
                    )['val']
                    cand = Candidate(
                        uid=-1,
                        rid=face_uuid,
                        tmpid=None,
                        features=norm_feat,
                        image=img64,
                        resourceID=resourceID)
                    logging.info("clustering uuid: " + face_uuid)

                    with lock:
                        clusterizer.clusterize_new_candidate(cand)
                        save_pickle(
                            'mounted_dir/clusterizer.pickle',
                            clusterizer)
                        clusters = clusterizer._query(cand, 5)
                        logging.info('clusters are retrieved ...')

                    reid_rank_top_5 = []
                    reid_rank_top_5_similarity = []
                    reid_rank_top_5_resource_id = []

                    for c in clusters:
                        closest_candidate_from_centroid = \
                            clusterizer.get_closest_candidate_from_centroid(
                                c.cluster_centroid, c.cands)
                        reid_rank_top_5.append(
                            closest_candidate_from_centroid.realID)
                        reid_rank_top_5_similarity.append(c.sim_score)
                        reid_rank_top_5_resource_id.append(
                            closest_candidate_from_centroid.resourceID)
                        logging.info(
                            f"similarity to cluster {c.pid} of person {c.pid}:\
                                {c.sim_score}")

                    # adding the reid result into the vcd
                    vcd_from_json.add_object_data(uid, types.vec(
                        "reid_rank_top_5",
                        reid_rank_top_5),
                        current_frame)
                    vcd_from_json.add_object_data(uid, types.vec(
                        "reid_rank_top_5_similarity",
                        reid_rank_top_5_similarity),
                        current_frame)
                    vcd_from_json.add_object_data(uid, types.vec(
                        "reid_rank_top_5_resourceID",
                        reid_rank_top_5_resource_id),
                        current_frame)

            json_data['output_data']['data'] = vcd_from_json.data
            json_data['processing_type'] = "FACE_RECOG"
            # output_json = dumps(json_data)
            # producer.send('results', value=output_json)
            # producer.flush()

        except Exception as ex:
            logging.exception(
                f'Exception occurred when saving the image: {str(ex)}')
    else:
        logging.info('There is no vcd in the json file')


@app.get("/API/CROP/{uuid}")
async def get_image_with_uuid(uuid: str):
    logging.info(divider)
    logging.info("searsh crop for uuid:" + uuid)
    logging.info("loading cluster")
    global clusterizer
    global lock
    with lock:
        img64 = clusterizer.get_image_from_candidates(uuid)
    logging.info(divider)
    return (img64)


@app.on_event("startup")
async def startup_event():
    global clusterizer
    global lock
    logging.info(divider)
    logging.info("main function start / subscribed to kafka")
    thread = Thread(target=subscribe_to_kafka_and_clusterize_all_images)
    thread.start()


if __name__ == '__main__':
    logging.info("FastApi is starting ...")
    uvicorn.run(app, host="0.0.0.0", port=9095)
