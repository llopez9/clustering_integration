import os
import cv2
import random

from torchreid.utils.feature_extractor import FeatureExtractor


class osNetExtractor:
    def __init__(self):
        folder_path = '/home/VICOMTECH/llopez/WorkSpace/Databases/Market-1501'

        # Define the folder containing the images
        input_path = os.path.join(folder_path, 'v15.09.15/bounding_box_train')

        # Load the feature extractor model
        self.extractor = FeatureExtractor(
            model_name='osnet_ain_x1_0',
            model_path='model/model.pth.tar',
            device='cpu'
        )

        # Loop over each folder in the specified directory
        for filename in os.listdir(input_path):
            # Check if the file is an image
            if not filename.endswith('.jpg') \
                and not filename.endswith('.png') \
                    and not filename.endswith('.jpeg'):
                continue

            # Open the image using OpenCV
            image_path = os.path.join(input_path, filename)
            image = cv2.imread(image_path)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

            # Extract features from the image
            features = self.extractor(image)

            self.images.append((features, filename.split('.')[0]))

    def get_random_FV(self):
        return random.choice(self.images)
