from clustering.clustering import Clusterizer, Cluster, Candidate
from clustering.utils import getBCubedFScore, parseMS1MCData
from clustering.adapter import getCandidates, retrieve_query

import numpy as np
import time


featuresFile = 'files_for_test/part9_test.bin'
labelsFile = 'files_for_test/part9_test.meta'

features, subjectId, query_feature, query_id = parseMS1MCData(
    featuresFile, labelsFile)

es = len(features)
es = np.array(features)
norm = np.sum(np.square(features), axis=1)
norm = np.sqrt(norm)
features /= norm[:, np.newaxis]
nSamples = len(features)

candidates = []
for i in range(nSamples):
    candidates.append(
        Candidate(
            userID=-1,
            realID=subjectId[i],
            features=features[i])
    )
fThr = [0.68]  # to decide whether two clusters should be fused together.
cThrL = [0.52]  # to connect a robust cluster with a non-robust one
cThrH = [0.74]  # to connect two robust clusters
# maximum number of connections allowed for a robust cluster
maxNConnections = [25]
minCand = [4]  # minimum number of samples to classify a cluster as robust
maxNClusters = 1000000
bestscore = 0.0
bestindex = 0
for t in range(len(fThr)):
    # for (size_t t = 0; t < fThr.size(); ++t) {
    print("fThr : {}, cThrL: {}".format(fThr[t], cThrL[t]))
    print("cThrH : {}".format(cThrH[t]))
    print("maxNConnections : {}".format(maxNConnections[t]))
    print("minCand : {}".format(minCand[t]))
    clusterizer = Clusterizer(
        max_clusters=maxNClusters,
        fusion_threshold=fThr[t],
        connection_threshold_low=cThrL[t],
        connection_threshold_high=cThrH[t],
        max_connections=maxNConnections[t],
        min_candidates=minCand[t]
    )
    initT = time.time()
    clusterizer.clusterize(candidates)
    endT = time.time()
    print("Elapsed time:{}s ".format(endT-initT))
    outCandidates = getCandidates(clusterizer)
    print("Filtering distractors...")
    filteredCandidates = []

    for i in range(len(outCandidates)):
        if (outCandidates[i].realID != -1):
            filteredCandidates.append(outCandidates[i])

    print("Samples after filtering: {}".format(len(filteredCandidates)))

    score = getBCubedFScore(filteredCandidates)
    # // getNMI(filteredCandidates);

    if (score > bestscore):
        bestscore = score
        bestindex = t

print("best score : {} ".format(bestscore))
print("best thr : ")
print("fThr: {}, cThrL: {}".format(fThr[bestindex], cThrL[bestindex]))
print("cThrH: {}".format(cThrH[bestindex]))
print("maxNConnections: {}".format(maxNConnections[bestindex]))
print("minCand: {}".format(minCand[bestindex]))

# test the retrieve function
clusters = retrieve_query(
    clusterizer,
    Candidate(userID=-1, realID=query_id, features=query_feature),
    k=3
)
for cluster in clusters:
    print("similarity to cluster {} of person {}: {}".format(
        cluster.cluster_id, cluster.person_id, cluster.similarity_score))
