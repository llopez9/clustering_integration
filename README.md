# Reidentification using Clustering 


## Build Docker image

```shell

$ cd Dockerfiles
$ sh build.sh

```

## Project Structure 
```shell
.
├── clustering
│   ├── clustering.py
│   └── utils.py
├── Dockerfiles
│   ├── build.sh
│   ├── Dockerfile
│   └── run.sh
├── main.py
├── README.md
└── requirements.txt

```
