import numpy as np


def gradient_descent(grad_f, x0, gamma, tol, maxit):
    x = x0
    for i in range(maxit):
        grad = grad_f(x)
        norm_grad = np.linalg.norm(grad)
        if norm_grad < tol:
            break
        x = x - gamma * grad
    return x


def grad_f(x):
    return 12 * x**3 + 12 * x**2 - 24 * x


x0 = 3
gamma = 0.1
tol = 1e-12
maxit = int(1e5)

result = gradient_descent(grad_f, x0, gamma, tol, maxit)
print("Aproximación de x que cumple f'(x) ≈ 0:", result)
