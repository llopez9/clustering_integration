from numpy import linalg as LA
import numpy as np
# import base64
# import cv2
# import io

#  size off the feature vector
VSIZE = 512  # initialy was 256


class Candidate:
    def __init__(
            self,
            features,
            userID='',
            realID='',
            tmpid='',
            image='',
            image_name=''
    ):
        """
        A class representing a candidate image.

        Args:
        user_id (int): The ID of the user associated with the image.
        real_id (int): The real ID of the image.
        template_id (int): The ID of the template associated with the image.
        features (np.ndarray): A numpy array of features associated
                               with the image.
        image (np.ndarray): A numpy array representing the image.
        """
        self.userID = userID
        self.realID = realID
        self.templateID = tmpid
        self.features = features
        self.image = image
        self.image_name = image_name


class Cluster:
    # the cluster identity, one person can have 1-multiple clusters
    # the candidate/image id, one cluster can have 1-multiple images(candidate)
    def __init__(
            self,
            similarity_score,
            person_id,
            cluster_centroid,
            cand_ids,
            cluster_id=''):
        """
        A class representing a cluster of candidate images.

        Args:
        similarity_score (float): The similarity score of the cluster.
        person_id (int): The ID of the person associated with the cluster.
        cluster_centroid (np.ndarray): A numpy array representing
                                       the centroid of the cluster.
        candidate_ids (list): A list of candidate IDs belonging to the cluster.
        """
        self.similarity_score = similarity_score
        self.person_id = person_id
        self.cluster_centroid = cluster_centroid
        self.cands = cand_ids
        self.cluster_id = cluster_id


class Clusterizer:
    def __init__(
            self,
            max_clusters=1000000,
            fusion_threshold=0.7,
            connection_threshold_low=0.3,
            connection_threshold_high=0.5,
            max_connections=10,
            min_candidates=4):
        """
        A class representing a clusterizer for candidate images.

        Args:
        max_clusters (int): The maximum number of clusters.
        fusion_threshold (float): The threshold for fusing clusters.
        connection_threshold_low (float): The lower threshold
                                          for connecting clusters.
        connection_threshold_high (float): The higher threshold
                                           for connecting clusters.
        max_connections (int): The maximum number of connections
                               for a cluster.
        min_candidates (int): The minimum number of candidates required
                              for a cluster.
        """
        self.candidates = []
        self.ncandidates = 0

        self.max_clusters = max_clusters
        self.num_clusters = 0

        self.centroid_buffer = 0.0
        self.centroid_matrix = np.zeros((1, VSIZE))

        self.max_id = 0
        self.max_num_connections = max_connections
        self.min_num_candidates = min_candidates

        # Parameters for clustering
        self.fusion_threshold = fusion_threshold
        self.connection_threshold_low = connection_threshold_low
        self.connection_threshold_high = connection_threshold_high

        # List of cluster IDs
        self.cluster_ids = []  # list, length = nMaxClusters
        # List of candidate IDs belonging to each cluster
        self.cluster_connections = []
        self.cluster_connect_similarity = []
        # List of cosine similarity values for each cluster
        self.cosine_similarity = []

        self.sum_centroids = []  # list, length = nMaxClusters
        # cluster_candidates:
        # number of row: all the generated clusters
        # each row: the candidates belong to that cluster
        self.cluster_candidates = []  # list of lists, length = nMaxClusters
        self.cluster_centroids = []

    def calculate_cosine_similarity(self, feat, centroids):
        # calculate the cosine similarity with each cluster centroid
        cosine_sims = []
        for centroid in centroids:
            dot_product = centroid * feat
            cosine_similarity = sum(dot_product)
            cosine_sims.append(cosine_similarity)
        return cosine_sims

    def get_candidate_cosine_similarity(self, feature):
        # Get the cosine similarity of a FV to each cluster centroid
        self.cosine_similarity = self.calculate_cosine_similarity(
            feature,
            self.cluster_centroids,
        )

    def get_cluster_cosine_similarity(self, idx):
        # Get the cosine similarity between the cluster centroid
        # at idx and all other cluster centroids
        self.cosine_similarity = self.calculate_cosine_similarity(
            self.cluster_centroids[idx],
            self.cluster_centroids,
        )

    def get_biggest_similarity(self, features):
        # Get the index and value of the cluster centroid with the highest
        # cosine similarity to a feature vector
        self.get_candidate_cosine_similarity(features)
        closest_cluster_index, biggest_similarity = max(
            enumerate(self.cosine_similarity),
            key=lambda x: x[1],
        )
        return closest_cluster_index, biggest_similarity

    def is_robust(self, cluster_index):
        # Check if a cluster has enough candidates to be considered robust
        return len(self.cluster_candidates[cluster_index]) \
            >= self.min_num_candidates

    def is_not_robust(self, cluster_index):
        # Check if a cluster has enough candidates to be considered robust
        return len(self.cluster_candidates[cluster_index]) \
            < self.min_num_candidates

    def clusterize(self, candidates):
        for candidate in candidates:
            self.clusterize_new_candidate(candidate)

    def clusterize_new_candidate(self, candidate: Candidate):
        if not self.candidates:
            self.candidates.append(candidate)
            self.ncandidates = 1
            self.create_cluster(0)  # // create first cluster
        else:
            self.candidates.append(candidate)
            self.ncandidates += 1
            # get the biggest similarity cluster
            most_similar_cluster_idx,  highest_similarity_score = \
                self.get_biggest_similarity(candidate.features)
            # If similarity is not high enough to fuse
            if highest_similarity_score < self.fusion_threshold:  # not fuse
                self.create_cluster(self.ncandidates-1)
                # to connect a robust cluster with a non-robust one,
                # and the cluster to be connected is robust
                # (has more than 4 samples)
                if (highest_similarity_score >= self.connection_threshold_low
                        and self.is_robust(most_similar_cluster_idx)):
                    self.connect_clusters(
                        self.num_clusters - 1, most_similar_cluster_idx)
                    self.check_num_connections(
                        most_similar_cluster_idx, self.max_num_connections)
            else:  # fuse this sample
                self.update_cluster(
                    candidate_index=self.ncandidates-1,
                    cluster_index=most_similar_cluster_idx)
                current_cluster_index = most_similar_cluster_idx
                while current_cluster_index != -1:
                    # get cosine similarity of current cluster
                    # to all cluster centroids
                    self.get_cluster_cosine_similarity(current_cluster_index)
                    # set the updated cluster to 0 (avoiding compare to itself)
                    self.cosine_similarity[current_cluster_index] = 0.0
                    connection_count = 0
                    while True:
                        # get the most similar cluster to the updated cluster
                        most_similar_cluster_idx = \
                            self.cosine_similarity.index(
                                max(self.cosine_similarity))
                        if (self.cosine_similarity[most_similar_cluster_idx]
                                >= self.fusion_threshold and (
                                self.is_not_robust(current_cluster_index) or
                                self.is_not_robust(most_similar_cluster_idx))):
                            current_cluster_index = self.fuse_clusters(
                                current_cluster_index,
                                most_similar_cluster_idx)
                            break

                        self.c_thr = self.connection_threshold_low
                        # to connect two robust clusters
                        if (self.is_robust(current_cluster_index) and
                                self.is_robust(most_similar_cluster_idx)):
                            self.c_thr = self.connection_threshold_high

                        # connect a robust cluster with a non-robust one
                        if (self.cosine_similarity[most_similar_cluster_idx]
                                >= self.c_thr and (
                                self.is_robust(current_cluster_index) or
                                self.is_robust(most_similar_cluster_idx))):

                            self.connect_clusters(
                                current_cluster_index,
                                most_similar_cluster_idx)

                            if self.is_not_robust(current_cluster_index):
                                self.check_connections(current_cluster_index)
                                current_cluster_index = -1
                                break

                            self.cosine_similarity[most_similar_cluster_idx]\
                                = -self.cosine_similarity[
                                    most_similar_cluster_idx]
                            connection_count += 1
                            if (connection_count >= self.max_num_connections):
                                self.check_connections(current_cluster_index)
                                current_cluster_index = -1
                                break

                            continue
                        else:
                            self.check_connections(current_cluster_index)
                            current_cluster_index = -1
                            break

    # rmid-> cluster id to be removed, upid-> cluster id to be merged
    def recursive_update(self, rmid,  upid):
        if (rmid == upid):
            return
        if rmid in self.cluster_ids:
            # std::distance(cId.begin(), rmiter)
            rmIdx = self.cluster_ids.index(rmid)
            self.cluster_ids[rmIdx] = upid
            for i in range(len(self.cluster_connections[rmIdx])):
                newRmId = self.cluster_connections[rmIdx][i]
                self.recursive_update(newRmId, upid)
        return

    def create_cluster(self, candidate_index):
        # Set the centroid matrix to the feature vector of the candidate
        self.centroid_matrix = self.candidates[candidate_index].features
        self.sum_centroids.append(self.candidates[candidate_index].features)
        # Create a new empty list for the current cluster
        self.cluster_candidates.append([])
        # Add the current candidate to the current cluster
        self.cluster_candidates[self.num_clusters].append(candidate_index)

        # Initialize the cluster's centroid matrix with the candidate's FV
        self.cluster_centroids.append(self.centroid_matrix)

        # Generate a unique ID for the new cluster
        self.max_id += 1
        self.cluster_ids.append(self.max_id)
        # Create an empty list of candidate IDs for the new cluster
        self.cluster_connections.append([])
        # Create an empty list of cosine similarities for the new cluster
        self.cluster_connect_similarity.append([])

        # Increment the number of clusters
        self.num_clusters += 1

    # findex-> to be fused sample index, cindex-> closest cluster index
    def update_cluster(self, candidate_index, cluster_index: int):
        # Add the candidate's feature vector to the sum of centroids
        # for the given cluster
        self.sum_centroids[cluster_index] += \
            self.candidates[candidate_index].features

        # Add the candidate's ID to the current cluster
        self.cluster_candidates[cluster_index].append(candidate_index)

        # Recalculate the centroid matrix for the given cluster
        self.centroid_matrix = self.sum_centroids[cluster_index] / \
            LA.norm(self.sum_centroids[cluster_index])
        self.cluster_centroids[cluster_index] = self.centroid_matrix

    def fuse_clusters(self, idx1,  idx2):
        # Determine which index is the "upper" cluster and
        # which is the "lower" cluster
        if idx1 > idx2:  # remove the latter cluster, update the former cluster
            up_idx = idx2
            rm_idx = idx1
        else:
            up_idx = idx1
            rm_idx = idx2
        # Add the sum of centroids and candidate lists for
        # the lower cluster to the upper cluster
        self.sum_centroids[up_idx] += self.sum_centroids[rm_idx]
        self.cluster_candidates[up_idx] += self.cluster_candidates[rm_idx]

        # Recalculate the centroid matrix for the upper cluster
        self.centroid_matrix = self.sum_centroids[up_idx] / \
            LA.norm(self.sum_centroids[up_idx])
        self.cluster_centroids[up_idx] = self.centroid_matrix

        # Add the IDs of the lower cluster's candidates to the upper cluster's
        # list of candidate IDs
        if (len(self.cluster_connections[rm_idx]) > 0):
            up_id = self.cluster_ids[up_idx]
            rm_id = self.cluster_ids[rm_idx]
            for i, candidate_id in \
                    enumerate(self.cluster_connections[rm_idx]):
                # If the candidate ID is already in the upper cluster's
                # list of candidate IDs, remove it
                if candidate_id == up_id:
                    tmp_idx1 = self.cluster_connections[up_idx].index(rm_id)
                    del self.cluster_connections[up_idx][tmp_idx1]
                    del self.cluster_connect_similarity[up_idx][tmp_idx1]
                else:
                    found = False
                    for j, id in enumerate(self.cluster_connections[up_idx]):
                        if candidate_id == id:
                            found = True
                            break

                    tmp_idx = self.cluster_ids.index(candidate_id)
                    tmp_idx1 = self.cluster_connections[tmp_idx].index(rm_id)

                    if found:
                        del self.cluster_connections[tmp_idx][tmp_idx1]
                        del self.cluster_connect_similarity[tmp_idx][tmp_idx1]
                    else:
                        self.cluster_connections[up_idx].append(candidate_id)
                        self.cluster_connect_similarity[up_idx].append(
                            self.cluster_connect_similarity[tmp_idx]
                            [tmp_idx1])
                        self.cluster_connections[tmp_idx][tmp_idx1] = up_id

        # Remove the old cluster from sum_centroids and cluster_connections
        self.sum_centroids[rm_idx] = self.sum_centroids[self.num_clusters - 1]
        self.sum_centroids.pop()
        self.cluster_candidates[rm_idx] = \
            self.cluster_candidates[self.num_clusters - 1]
        self.cluster_candidates.pop()

        # Remove the old cluster from
        #   cluster_centroids,
        #   cluster_ids,
        #   cluster_connections, and
        #   cluster_connect_similarity
        self.cluster_centroids[rm_idx] = \
            self.cluster_centroids[self.num_clusters - 1]
        self.cluster_centroids.pop()
        self.cluster_ids[rm_idx] = self.cluster_ids[self.num_clusters - 1]
        self.cluster_ids.pop()
        self.cluster_connections[rm_idx] = \
            self.cluster_connections[self.num_clusters - 1]
        self.cluster_connections.pop()
        self.cluster_connect_similarity[rm_idx] = \
            self.cluster_connect_similarity[self.num_clusters - 1]
        self.cluster_connect_similarity.pop()

        # Pop the old cluster from the lists
        self.num_clusters -= 1
        return up_idx

    # updated cluster, most similar cluster
    def connect_clusters(self, idx1, idx2):
        # Check if the clusters are already connected
        if self.cluster_ids[idx2] in self.cluster_connections[idx1]:
            return

        # Add the cluster ids and cosine similarities
        # to each other's candidate lists
        self.cluster_connections[idx1].append(self.cluster_ids[idx2])
        self.cluster_connections[idx2].append(self.cluster_ids[idx1])
        self.cluster_connect_similarity[idx1].append(
            self.cosine_similarity[idx2])
        self.cluster_connect_similarity[idx2].append(
            self.cosine_similarity[idx2])

    def check_connections(self, cluster_index):
        # If the cluster has no candidate connections, return
        if len(self.cluster_connections[cluster_index]) <= 0:
            return

        # Get the id of the current cluster
        cluster_id = self.cluster_ids[cluster_index]

        # Iterate through the candidate connections in reverse order
        for i in range(len(self.cluster_connections[cluster_index])-1, -1, -1):
            # Get the id of the candidate cluster and its index
            candidate_id = self.cluster_connections[cluster_index][i]
            candidate_idx = self.cluster_ids.index(candidate_id)

            # If the cosine similarity is negative, flip its sign
            if self.cosine_similarity[candidate_idx] < 0.0:
                self.cosine_similarity[candidate_idx] = - \
                    self.cosine_similarity[candidate_idx]

            # Update connections cosine_similarity
            tmp_idx = self.cluster_connections[candidate_idx].index(
                cluster_id)
            self.cluster_connect_similarity[cluster_index][i] = \
                self.cosine_similarity[candidate_idx]
            self.cluster_connect_similarity[candidate_idx][tmp_idx] = \
                self.cosine_similarity[candidate_idx]

            # Set the connection threshold based on the number of
            # candidate connections in each cluster
            self.c_thr = self.connection_threshold_low
            if self.is_robust(cluster_index) and self.is_robust(candidate_idx):
                self.c_thr = self.connection_threshold_high

            # If the cosine similarity is below the threshold,
            # remove the candidate connection
            if self.cosine_similarity[candidate_idx] < self.c_thr:
                del self.cluster_connections[cluster_index][i]
                del self.cluster_connect_similarity[cluster_index][i]
                del self.cluster_connections[candidate_idx][tmp_idx]
                del self.cluster_connect_similarity[candidate_idx][tmp_idx]
            # Otherwise, check the number of connections
            # in the candidate cluster
            else:
                tmp_max_n_conn = self.max_num_connections
                if self.is_not_robust(candidate_idx):
                    tmp_max_n_conn = 1
                self.check_num_connections(candidate_idx, tmp_max_n_conn)

        # self.check_num_connections(cluster_index, self.max_num_connections)if
        #     self.is_not_robust(cluster_index) else\
        #     self.check_num_connections(cluster_index, 1)

        # Check the number of connections in the current cluster
        tmp_max_n_conn = self.max_num_connections
        if self.is_not_robust(cluster_index):
            tmp_max_n_conn = 1
        self.check_num_connections(cluster_index, tmp_max_n_conn)

    def check_num_connections(self, cluster_index, maxN):
        # Get the id of the cluster
        cluster_id = self.cluster_ids[cluster_index]
        self.get_cluster_cosine_similarity(cluster_index)
        # Remove clusters connections until the maximum number is reached
        while len(self.cluster_connections[cluster_index]) > maxN:
            minCosSim = 10.0
            minIdx = 0
            mini = 0
            # Iterate through the candidate connections to find the one
            for i in range(len(self.cluster_connections[cluster_index])):
                checkid = self.cluster_connections[cluster_index][i]
                checkidx = self.cluster_ids.index(checkid)
                if self.cosine_similarity[checkidx] < minCosSim:
                    minCosSim = self.cosine_similarity[checkidx]
                    minIdx = checkidx
                    mini = i
            del self.cluster_connections[cluster_index][mini]
            del self.cluster_connect_similarity[cluster_index][mini]
            tmpIdx = self.cluster_connections[minIdx].index(cluster_id)
            del self.cluster_connections[minIdx][tmpIdx]
            del self.cluster_connect_similarity[minIdx][tmpIdx]
