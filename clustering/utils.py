# include "utils.h"
# from tkinter import NS
# from weakref import ref
# from pyparsing import nums
import numpy as np
import math
VSIZE = 256  # feature embedding size (saved in the bin file)


def getBCubedFScore(inCand):
    # float getBCubedFScore(std::vector<Candidate> inCand){
    # calculate bcubed f-score
    nSamples = len(inCand)
    clusters = []
    refclusters = []
    # std::vector<std::vector<Candidate>> clusters, refclusters;
    while (len(inCand) > 0):
        cand = inCand[-1]  # inCand.back()
        found = False
        for i in range(len(clusters)):
            if cand.userID == clusters[i][0].userID:
                found = True
                clusters[i].append(cand)
                break

        if not found:
            clusters.append([])
            clusters[-1].append(cand)

        found = False
        for i in range(len(refclusters)):
            if cand.realID == refclusters[i][0].realID:
                found = True
                refclusters[i].append(cand)
                break

        if not found:
            refclusters.append([])
            refclusters[-1].append(cand)
        inCand.pop()
    print("Num clusters: {}".format(len(clusters)))
    print("Num real clusters: {}".format(len(refclusters)))

    precision = 0.0
    for cluster in clusters:
        positives = 0.0
        for sample in cluster:
            for sample1 in cluster:
                if (sample.realID == sample1.realID):
                    positives += 1
        precision += positives / len(cluster)
    precision /= nSamples

    recall = 0.0
    for cluster in refclusters:
        positives = 0.0
        for sample in cluster:
            for sample1 in cluster:
                if (sample.userID == sample1.userID):
                    positives += 1
        recall += positives / len(cluster)
    recall /= nSamples

    fscore = 2.0 * precision * recall / (precision + recall)
    print("Precision: {:.2f}%, Recall: {:.2f}%, F-Score:{:.2f}".format(
        precision*100, recall*100, fscore))
    return fscore


def parseData(infile):
    features = []
    subjectID = []
    with open(infile) as file:
        if not file:
            print("Couldn't load {}".format(infile))
            return
        else:
            lines = file.readlines()
    print("Parsing ground truth data file...")
    for line in lines:
        substrs = line.split(' ')
        data = [substr for substr in substrs]

        # reserve this space: std::vector<float>(VSIZE)
        features.append([] * VSIZE)
        for i in range(VSIZE):
            features[-1][i] = float(data[i])
        subjectID.append(int(data[VSIZE]))
    print("Done.")
    return features, subjectID


def parseIjbCData(infile):
    features = []
    templateid = []
    subjectid = []
    with open(infile) as file:
        if not file:
            print("Couldn't load {}".format(infile))
            return
        else:
            lines = file.readlines()
    print("Parsing ground truth data file...")

    for line in lines:
        # while (getline(file, line)) {
        # std::vector<std::string> data;
        substrs = line.split(' ')
        data = [substr for substr in substrs]

        features.append(VSIZE)
        # features->push_back(std::vector<float>(VSIZE));
        for i in range(VSIZE):
            features[-1][i] = float(data[i])
        templateid.append(int(data[VSIZE]))
        subjectid.append(int(data[VSIZE+3]))
    print("Done.")
    return features, templateid, subjectid


def parseMS1MCData(featuresFile, labelsFile):
    features = []
    subjectId = []
    with open(labelsFile) as file:
        if not file:
            print("Couldn't load {}".format(labelsFile))
            return
        else:
            lines = file.readlines()
    print("Parsing ground truth data file...")
    subjectId = np.array(lines, dtype=int)

    # N = len(subjectId) * VSIZE
    feat_buf = []

    with open(featuresFile, mode='rb') as file:  # b is important -> binary
        # the format float32 is correct, float64 is wrong
        feat_buf = np.fromfile(featuresFile,  dtype=np.float32)
    for i in range(len(subjectId)):
        start = i*VSIZE
        features.append(feat_buf[start:start+VSIZE])
    return features[:1500], subjectId[:1500], features[1501], subjectId[1501]

    # return features, subjectId


def parseVGG2(infile, labelfile, nSamples):
    features = []
    subjectid = []
    with open(infile) as file:
        if not file:
            print("Couldn't load {}".format(infile))
            return
        else:
            lines = file.readlines()

    print("Parsing ground truth data file...")
    numSamples = 0
    for line in lines:
        if numSamples < nSamples:
            substrs = line.split(' ')
            data = [substr for substr in substrs]
        features.append([]*VSIZE)
        for i in range(VSIZE):
            features[-1][i] = float(data[i])
        numSamples += 1

    numSamples = 0
    with open(labelfile) as file:
        if not file:
            print("Couldn't load {}".format(labelfile))
            return
        else:
            lines = file.readlines()

    for line in lines:
        if numSamples < nSamples:
            subjectid.append(int(line))
            numSamples += 1

    print("Done")
    return features, subjectid


def addDistractors(infile, nDistractors):
    features = []
    subjectid = []
    with open(infile) as file:
        if not file:
            print("Couldn't load {}".format(infile))
            return
        else:
            lines = file.readlines()
    print("Parsing ground truth data file...")
    numSamples = 0
    for line in lines:
        if numSamples < nDistractors:
            substrs = line.split(' ')
            data = [substr for substr in substrs]
        # features->push_back(std::vector<float>(VSIZE))
        features.append([]*VSIZE)
        for i in range(VSIZE):
            features[-1][i] = float(data[i])
        subjectid.append(-1)
        numSamples += 1
    print("Done")
    return features, subjectid


def getNMI(inCand):
    # float getNMI(std::vector<Candidate> inCand) {
    #  calculate bcubed f-score
    nSamples = len(inCand)
    clusters = [[]]
    refclusters = [[]]
    # std::vector<std::vector<Candidate>> clusters, refclusters;
    while (inCand.size() > 0):
        cand = inCand[-1]
        found = False
        for i in range(len(clusters)):
            if cand.userID == clusters[i][0].userID:
                found = True
                clusters[i].push_back(cand)
                break

        if not found:
            clusters.append([])
            clusters[-1].append(cand)

        found = False
        for i in range(len(refclusters)):
            if (cand.realID == refclusters[i][0].realID):
                found = True
                refclusters[i].append(cand)
                break

        if not found:
            refclusters.append([])
            refclusters[-1].append(cand)
        inCand.pop()
    print("Num clusters: {}".format(len(clusters)))
    print("Num real clusters: {}".format(len(refclusters)))

    gtEntropy = 0.0
    for i in range(len(refclusters)):
        fraction = len(refclusters[i])/nSamples
        gtEntropy -= fraction*math.log(fraction)

    clEntropy = 0.0
    condEntropy = gtEntropy
    for i in range(len(clusters)):
        fraction = float(len(clusters[i])) / nSamples
        clEntropy -= fraction * math.log(fraction)

        foundIds = []  # std::vector<int>
        numIdsMembers = []  # std::vector<int>
        for j in range(len(clusters[i])):
            found = False
            for k in range(len(foundIds)):
                if (clusters[i][j].realID == foundIds[k]):
                    found = True
                    numIdsMembers[k] += 1
                    break

            if not found:
                foundIds.append(clusters[i][j].realID)
                numIdsMembers.append(1)

        tmpCondEntropy = 0.0
        for k in range(len(numIdsMembers)):
            tmpFraction = float(numIdsMembers[k]) / len(clusters[i])
            tmpCondEntropy += tmpFraction * math.log(tmpFraction)

        tmpCondEntropy *= -fraction
        condEntropy -= tmpCondEntropy

    nmi = 2 * condEntropy / (gtEntropy + clEntropy)
    print("gtEntropy   clEntropy   condEntropy   nmi")
    print("{}   {}   {}   {}".format(gtEntropy, clEntropy, condEntropy, nmi))
    return nmi
