import cv2
import numpy as np
import string
import base64
import io
import os

from imageio import imread
# from numpy import linalg as LA
from pathlib import Path
from clustering.clustering import Candidate, Cluster

SAVE_REID = os.getenv("SAVE_REID")
if not SAVE_REID:
    SAVE_REID = False
SAVE_REID = int(SAVE_REID)


def make_a_border(img, equivalence, score):
    if equivalence == 'same':
        color = [0, 255, 0]
    elif equivalence == 'diferent':
        color = [0, 0, 255]
    else:
        color = [126, 126, 126]
    bordersize = 5
    border = cv2.copyMakeBorder(
        img,
        top=bordersize,
        bottom=bordersize,
        left=bordersize,
        right=bordersize,
        borderType=cv2.BORDER_CONSTANT,
        value=color
    )
    ubicacion = (10, 30)
    font = cv2.FONT_HERSHEY_SIMPLEX
    fontScale = 1
    fontWidth = 2
    cv2.putText(border, score, ubicacion, font, fontScale, color, fontWidth)
    return border


def getCandidates(clusterizer):
    # print("num of centroids generated: {}".format(clusterizer.num_clusters))
    processedClusters = []
    clusterizer.cluster_ids = clusterizer.cluster_ids.copy()
    # clusterizer.cId(generated cluster ids) will be overwritten
    # to generated identities ids (merge all the connected clusters)
    # so here we copy the originally generated cluster ids to keep track
    # of different clusters that belong to one identity
    # for (size_t i = 0; i < nClusters; ++i):
    for i in range(clusterizer.num_clusters):
        upid = clusterizer.cluster_ids[i]
        processed = False
        for j in range(len(processedClusters)):
            if (upid == processedClusters[j]):
                processed = True
                break
        if (processed):
            continue
        for j in range(len(clusterizer.cluster_connections[i])):
            rmid = clusterizer.cluster_connections[i][j]
            clusterizer.recursive_update(rmid, upid)
        processedClusters.append(upid)

    for i in range(clusterizer.num_clusters):
        # iterate through all the generated centroids
        for j in range(len(clusterizer.cluster_candidates[i])):
            clusterizer.candidates[clusterizer.cluster_candidates[i][j]
                                   ].userID = clusterizer.cluster_ids[i]
    return clusterizer.candidates


def get_image_from_candidates(clusterizer, uuid: str):
    for c in range(len(clusterizer.candidates)):
        if (clusterizer.candidates[c].realID == uuid):
            return clusterizer.candidates[c].image
    return []


def retrieve_query(clusterizer, query_cand, k):
    # args:
    #   query_cand: query in candidate structure,
    #   k: the number of identities we want to retrieve

    # return:
    #   a list of clusteres with the corresponding:
    #       similarity score,
    #       person ID,
    #       cluster ID,
    #       image IDs

    # eliminate empty centroids
    clusterizer.cluster_centroids = [
        c for c in clusterizer.cluster_centroids if len(c)]

    cosSims = clusterizer.calculate_cosine_similarity(
        query_cand.features,
        clusterizer.cluster_centroids
    )
    cos = cosSims.copy()
    cos.sort()

    person_ids = []
    clusters = []
    while len(set(person_ids)) < k:
        # get the k most similar centroids to the query vector
        cluster_similarity_score = cos.pop()
        # the person identity
        upidx = cosSims.index(cluster_similarity_score)
        person_ids.append(clusterizer.cluster_ids[upidx])
        curr_cluster = Cluster(
            similarity_score=cluster_similarity_score,
            person_id=clusterizer.cluster_ids[upidx],
            cluster_id=clusterizer.cluster_ids[upidx],
            cand_ids=clusterizer.cluster_candidates[upidx],
            cluster_centroid=''
        )
        clusters.append(curr_cluster)

    return clusters


def _query(clusterizer, query_cand: Candidate, k):
    # args:
    #   query_cand: query in candidate structure,
    #   k: the number of identities we want to retrieve

    # return:
    #   a list of clusteres with the corresponding:
    #       similarity score,
    #       person ID,
    #       cluster ID,
    #       image IDs

    # eliminate empty centroids
    clusterizer.cluster_centroids = [
        c for c in clusterizer.cluster_centroids if len(c)]

    cosSims = clusterizer.calculate_cosine_similarity(
        query_cand.features,
        clusterizer.cluster_centroids)
    cos = cosSims.copy()
    cos.sort()

    person_ids = []
    clusters = []
    while len(set(person_ids)) < min(k, len(clusterizer.cluster_centroids)):
        # get the k most similar centroids to the query vector
        cluster_similarity_score = cos.pop()
        # the person identity
        upidx = cosSims.index(cluster_similarity_score)
        person_ids.append(clusterizer.cluster_ids[upidx])
        curr_cluster = Cluster(
            similarity_score=cluster_similarity_score,
            person_id=clusterizer.cluster_ids[upidx],
            cluster_centroid=clusterizer.cluster_centroids[upidx],
            cand_ids=clusterizer.cluster_candidates[upidx]
        )
        clusters.append(curr_cluster)

    if SAVE_REID:
        image_query = query_cand.image
        image_query = imread(io.BytesIO(base64.b64decode(image_query)))
        image_query = cv2.cvtColor(image_query, cv2.COLOR_RGB2BGR)
        image_concate = make_a_border(image_query, 'neutral', "")
        for c in clusters:
            identities_cluster_candidates = []
            img_names_cluster_candidates = []
            for id_c_cand in c.cands:
                cluster_candidate = clusterizer.candidates[id_c_cand]
                identities_cluster_candidates.append(
                    cluster_candidate.realID)
                img_names_cluster_candidates.append(
                    cluster_candidate.image)

            if query_cand.realID in identities_cluster_candidates:
                value_index = identities_cluster_candidates.index(
                    query_cand.realID)
                image_cluster = img_names_cluster_candidates[value_index]
                image_cluster = imread(io.BytesIO(
                    base64.b64decode(image_cluster)))
                image_cluster = cv2.cvtColor(
                    image_cluster, cv2.COLOR_RGB2BGR)

                if (c.sim_score < clusterizer.fusion_threshold):
                    img_grey = cv2.cvtColor(
                        image_cluster, cv2.COLOR_BGR2GRAY)
                    image_cluster = cv2.cvtColor(
                        img_grey, cv2.COLOR_GRAY2BGR)
                img_with_border = make_a_border(
                    image_cluster, 'same', str(round(c.sim_score, 2)))
            else:
                image_cluster = get_closest_candidate_from_centroid(
                    clusterizer,
                    c.cluster_centroid,
                    c.cands).image
                image_cluster = imread(io.BytesIO(
                    base64.b64decode(image_cluster)))
                image_cluster = cv2.cvtColor(
                    image_cluster, cv2.COLOR_RGB2BGR)
                if (c.sim_score < clusterizer.fusion_threshold):
                    img_grey = cv2.cvtColor(
                        image_cluster, cv2.COLOR_BGR2GRAY)
                    image_cluster = cv2.cvtColor(
                        img_grey, cv2.COLOR_GRAY2BGR)
                img_with_border = make_a_border(
                    image_cluster, 'diferent', str(round(c.sim_score, 2)))
            image_concate = np.concatenate(
                (image_concate, img_with_border), axis=1)
        # image_concate = cv2.cvtColor(image_concate, cv2.COLOR_BGR2RGB)
        cv2.imwrite(
            f'/code/mounted_dir/{query_cand.realID}.jpg', image_concate)
    return clusters


def saveClustersInImages(clusterizer, path: string):
    for c in range(clusterizer.num_clusters):
        for i in clusterizer.cluster_candidates[c]:
            image = base64.b64decode(clusterizer.candidates[i].image)
            image = np.fromstring(image, np.uint8)
            image = cv2.imdecode(image, cv2.IMREAD_COLOR)
            dpath = path + '/' + str(clusterizer.cluster_ids[c]) + '/'
            Path(dpath).mkdir(parents=True, exist_ok=True)
            clsuterize_canditate = str(clusterizer.candidates[i].realID)
            wpath = f'{dpath}{str(i)}_{clsuterize_canditate}.jpg'
            # Saving the image
            cv2.imwrite(wpath, image)


def saveClusterInImagesWithScore(clusterizer, path: string):
    print("start method")
    for c in range(clusterizer.num_clusters):
        idx = 0
        for i in clusterizer.cluster_candidates[c]:
            dot = clusterizer.cluster_centroids[c] * \
                clusterizer.candidates[i].features
            CosSims = sum(dot)
            image = base64.b64decode(clusterizer.candidates[i].image)
            image = np.fromstring(image, np.uint8)
            image = cv2.imdecode(image, cv2.IMREAD_COLOR)
            # image = cv2.cvtColor(image, cv2.COLOR_RGB2BGR)
            dpath = path + '/' + str(clusterizer.cluster_ids[c]) + '/'
            print(dpath)
            Path(dpath).mkdir(parents=True, exist_ok=True)
            wpath = f'{dpath}{str(CosSims)}_{str(idx)}.jpg'
            # Saving the image
            cv2.imwrite(wpath, image)
            idx += 1
    return


def saveClusterImagesCentroids(clusterizer, cant: int, path: string):
    for c in range(clusterizer.num_clusters):
        CosSims = []
        for i in clusterizer.cluster_candidates[c]:
            dot = clusterizer.cluster_centroids[c] * \
                clusterizer.candidates[i].features
            CosSims.append(sum(dot))
        sortedCosSim = sorted(CosSims, reverse=True)
        idx = 0
        while idx < cant and idx < len(sortedCosSim):
            maxIdx = CosSims.index(sortedCosSim[idx])
            currentCandidate = \
                clusterizer.candidates[clusterizer.cluster_candidates
                                       [c][maxIdx]]
            image = base64.b64decode(currentCandidate.image)
            image = np.fromstring(image, np.uint8)
            image = cv2.imdecode(image, cv2.IMREAD_COLOR)
            dpath = path + '/' + str(clusterizer.cluster_ids[c]) + '/'
            Path(dpath).mkdir(parents=True, exist_ok=True)
            wpath = f'{dpath}{str(currentCandidate.realID)}_{str(idx)}.jpg'
            # Saving the image
            cv2.imwrite(wpath, image)
            idx += 1
    return


def get_closest_candidate_from_centroid(clusterizer,
                                        cluster_centroid,
                                        cand_list):
    CosSims = []
    for i in cand_list:
        dot = cluster_centroid*clusterizer.candidates[i].features
        CosSims.append(sum(dot))
    sortedCosSim = sorted(CosSims, reverse=True)
    maxIdx = CosSims.index(sortedCosSim[0])
    return clusterizer.candidates[cand_list[maxIdx]]


def get_clusters_for_new_feature(clusterizer, feature, nTop):
    # get the cosSim of curr sample to existing cluster centroids
    clusterizer.get_candidate_cosine_similarity(feature)
    # get the highest similarity values
    result = sorted(clusterizer.cosine_similarity)
    maxIdx_nTop = []
    for i in range(min(nTop, clusterizer.num_clusters)):
        # get the top N similarity cluster
        maxIdx_nTop.append(clusterizer.cosine_similarity.index(result[i]))
    return maxIdx_nTop
